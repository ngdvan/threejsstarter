import * as THREE from 'three';
import { FlyControls } from 'three/addons/controls/FlyControls.js';
// Scence
const scene = new THREE.Scene();
let container;
// renderer DOM element 
const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
container = document.createElement( 'div' );
document.body.appendChild( container );
container.appendChild( renderer.domElement );


// Camera setup
const camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 150 );
scene.add( camera );
camera.position.set( 0, 100, 100 );
camera.lookAt( 0, 0, 0 );

const clock = new THREE.Clock();
let controls;

controls = new FlyControls( camera, renderer.domElement );
controls.movementSpeed = 10;
controls.domElement = container;
controls.rollSpeed = Math.PI / 12;
controls.autoForward = false;
controls.dragToLook = false;

// Cube
const cubeGeometry = new THREE.BoxGeometry( 2, 2, 2 );
const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var basicMaterial = new THREE.MeshPhongMaterial({
    color: 0x0095DD
});
const cube = new THREE.Mesh( cubeGeometry, basicMaterial );
cube.rotateZ(20)
cube.rotateX(20)
scene.add( cube );

var Alight = new THREE.AmbientLight(0xf6e86d);
scene.add(Alight);

// var Dlight = new THREE.DirectionalLight(0x404040, 0.1);
// scene.add(Dlight);

var light = new THREE.HemisphereLight(0x404040, 0xFFFFFF, 0.5);
scene.add(light);

const spotLight = new THREE.SpotLight( 0xffffff );
spotLight.map = new THREE.TextureLoader().load( 'disturb.jpg' );
spotLight.position.set( 10, 10, 10 );

spotLight.castShadow = true;

spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;

spotLight.shadow.camera.near = 500;
spotLight.shadow.camera.far = 4000;
spotLight.shadow.camera.fov = 30;

scene.add( spotLight );
// controls.update();

function animate() {
	requestAnimationFrame( animate );
    render();
	renderer.render( scene, camera );
}

function render() {

	const delta = clock.getDelta();

	controls.update( delta );
	renderer.render( scene, camera );

}
animate();